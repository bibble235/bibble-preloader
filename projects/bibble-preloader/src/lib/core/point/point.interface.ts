type IPointConstructor = new (x: number, y: number) => IPoint

export interface IPoint {
  x: number
  y: number
}

export function createPoint(
  ctor: IPointConstructor,
  x: number,
  y: number
): IPoint {
  return new ctor(x, y)
}
