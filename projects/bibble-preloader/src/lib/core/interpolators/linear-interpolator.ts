import { IInterpolator } from './interpolator.interface'

export class LinearInterpolator implements IInterpolator {
  constructor() {}

  getInterpolar(t: number): number {
    return t
  }
}
