import { IInterpolator } from './interpolator.interface'

export class AccelerateDecelerateInterpolator implements IInterpolator {
  constructor() {}

  getInterpolar(t: number): number {
    return Math.cos((t + 1) * Math.PI) / 2.0 + 0.5
  }
}
