export interface IInterpolator {
  getInterpolar(t: number): number
}
