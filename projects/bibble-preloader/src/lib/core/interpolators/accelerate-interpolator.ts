import { IInterpolator } from './interpolator.interface'

export class AccelerateInterpolator implements IInterpolator {
  factor: number

  constructor(factor: number = 2.0) {
    this.factor = factor
  }

  getInterpolar(t: number): number {
    return Math.pow(t, 2 * this.factor)
  }
}
