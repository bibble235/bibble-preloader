import { Point } from '../point/point'
import { createPoint, IPoint } from '../point/point.interface'
import { Direction } from './direction'

export class CoordinateGenerator {
  arcHeight: number
  arcWidth: number
  centerX: number
  centerY: number
  circleRadius: number
  endX: number
  endY: number
  startX: number
  startY: number

  constructor(
    circleRadius,
    canvasWidth,
    canvasHeight,
    arcWidth,
    arcHeight: number
  ) {
    this.circleRadius = circleRadius
    this.arcWidth = arcWidth
    this.arcHeight = arcHeight

    this.setQuadValues(canvasWidth, canvasHeight)
  }

  getCordinate(percentComplete: number): IPoint {
    let point: Point = null

    if (percentComplete < 50) {
      const percent: number = percentComplete / 49
      point = this.getQuadraticBezierXYatPercent(Direction.down, percent)
    } else {
      const percent: number = (percentComplete - 50) / 49
      point = this.getQuadraticBezierXYatPercent(Direction.up, percent)
    }

    return point
  }

  private getQuadraticBezierXYatPercent(
    direction: Direction,
    percent: number
  ): IPoint {
    let startPoint: IPoint = null
    let controlPoint: IPoint = null
    let endPoint: IPoint = null

    if (direction === Direction.down) {
      startPoint = createPoint(Point, this.startX, this.startY)
      controlPoint = createPoint(
        Point,
        this.centerX - this.arcHeight,
        this.startY
      )
      endPoint = createPoint(Point, this.centerX, this.centerY)
    } else if (direction === Direction.up) {
      startPoint = createPoint(Point, this.centerX, this.centerY)
      controlPoint = createPoint(
        Point,
        this.centerX + this.arcHeight,
        this.endY
      )
      endPoint = createPoint(Point, this.endX + this.arcWidth, this.endY)
    }

    const x: number =
      Math.pow(1 - percent, 2) * startPoint.x +
      2 * (1 - percent) * percent * controlPoint.x +
      Math.pow(percent, 2) * endPoint.x

    const y: number =
      Math.pow(1 - percent, 2) * startPoint.y +
      2 * (1 - percent) * percent * controlPoint.y +
      Math.pow(percent, 2) * endPoint.y

    return createPoint(Point, x, y)
  }

  private setQuadValues(canvasWidth, canvasHeight: number): void {
    this.centerX = canvasWidth / 2
    this.centerY = canvasHeight / 2
    this.startX = 0
    this.startY = 0
    this.endX = canvasWidth
    this.endY = 0
  }
}
