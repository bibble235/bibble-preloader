import { Point } from '../point/point'
import { createPoint, IPoint } from '../point/point.interface'
import { Circle } from './circle'
import { createCircle } from './circle-interface'
describe('Circle', () => {
  it('test 1', () => {
    const point: IPoint = createPoint(Point, 0, 1)
    const circle = createCircle(Circle, point, 10, `#222`)
    expect(circle.color).toEqual(`#222`)
    expect(circle.point.x).toEqual(0)
    expect(circle.point.y).toEqual(1)
    expect(circle.radius).toEqual(10)
  })
})
