import { IPoint } from '../point/point.interface'
import { ICircle } from './circle-interface'

export class Circle implements ICircle {
  color: string
  point: IPoint
  radius: number

  constructor(point: IPoint, radius: number, color: string) {
    this.point = point
    this.radius = radius
    this.color = color
  }
}
