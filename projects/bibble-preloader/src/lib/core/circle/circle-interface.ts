import { IPoint } from '../point/point.interface'

type ICircleConstructor = new (
  point: IPoint,
  radius: number,
  color: string
) => ICircle

export interface ICircle {
  color: string
  point: IPoint
  radius: number
}

export function createCircle(
  ctor: ICircleConstructor,
  point: IPoint,
  radius: number,
  color: string
): ICircle {
  return new ctor(point, radius, color)
}
