import { Point } from '../point/point'
import { createPoint, IPoint } from '../point/point.interface'
import { createBottomLine, IBottomLine } from './bottom-line.interface'

export class BottomLine implements IBottomLine {
  static fromCoordinates(
    point0x: number,
    point0y: number,
    point1x: number,
    point1y: number,
    lineWidth: number,
    color: string
  ): IBottomLine {
    return createBottomLine(
      BottomLine,
      createPoint(Point, point0x, point0y),
      createPoint(Point, point1x, point1y),
      lineWidth,
      color
    )
  }
  color: string
  lineWidth: number
  point0: IPoint
  point1: IPoint

  constructor(
    point0: IPoint,
    point1: IPoint,
    lineWidth: number,
    color: string
  ) {
    this.point0 = point0
    this.point1 = point1
    this.lineWidth = lineWidth
    this.color = color
  }
}
