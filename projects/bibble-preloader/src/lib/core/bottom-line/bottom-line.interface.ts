import { IPoint } from '../point/point.interface'

type IBottomLineConstructor = new (
  point0: IPoint,
  point1: IPoint,
  lineWidth: number,
  color: string
) => IBottomLine

export interface IBottomLine {
  color: string
  lineWidth: number
  point0: IPoint
  point1: IPoint
}

export function createBottomLine(
  ctor: IBottomLineConstructor,
  point0: IPoint,
  point1: IPoint,
  lineWidth: number,
  color: string
): IBottomLine {
  return new ctor(point0, point1, lineWidth, color)
}
