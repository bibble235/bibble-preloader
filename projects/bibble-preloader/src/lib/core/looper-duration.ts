export class LooperDuration {
  loopDuration: number
  loopElapsedTime: number
  loopInterval: number

  constructor(loopDuration: number, loopInterval: number) {
    this.loopDuration = loopDuration
    this.loopInterval = loopInterval
    this.loopElapsedTime = 0
  }

  gett(): number {
    return this.loopElapsedTime / this.loopDuration
  }

  gettWithTWithElapsedTime(elapsedTime: number): number {
    return elapsedTime / this.loopDuration
  }

  updateLoop(): void {
    this.loopElapsedTime = this.loopElapsedTime + this.loopInterval

    if (this.loopElapsedTime >= this.loopDuration) {
      this.loopElapsedTime = 0
    }
  }
}
