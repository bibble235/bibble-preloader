import { ComponentFixture, TestBed } from '@angular/core/testing'

import { BibblePreloaderComponent } from './bibble-preloader.component'

describe('BibblePreloaderComponent', () => {
  let component: BibblePreloaderComponent
  let fixture: ComponentFixture<BibblePreloaderComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BibblePreloaderComponent],
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(BibblePreloaderComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
