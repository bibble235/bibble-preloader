import {
  AfterViewInit,
  Component,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core'

import { IBottomLine } from '../core/bottom-line/bottom-line.interface'
import { createCircle, ICircle } from '../core/circle/circle-interface'
import { IPoint } from '../core/point/point.interface'

import { AccelerateDecelerateInterpolator } from '../core/interpolators/accelerate-decelerate-Interpolator'
import { IInterpolator } from '../core/interpolators/interpolator.interface'

import { interval, Subscription } from 'rxjs'
import { LooperDuration } from '../core/utilities/looper-duration'
import { CoordinateGenerator } from '../core/utilities/coordinate-generator'
import { BottomLine } from '../core/bottom-line/bottom-line'
import { Circle } from '../core/circle/circle'

@Component({
  selector: 'lib-bibble-preloader',
  templateUrl: 'bibble-preloader.component.html',
  styleUrls: ['bibble-preloader.component.css'],
})
export class BibblePreloaderComponent
  implements OnInit, AfterViewInit, OnDestroy {
  animateSubscription: Subscription
  animationDuration: number
  @Input() ArcHeight = 40

  @Input() ArcWidth = 75
  @Input() BallColor = '#000000'
  @Input() BallNumber = 3
  @Input() BallRadius = 10

  @Input() BottomLineColor = '#000000'
  @Input() BottomLineDelay = 10
  @Input() BottomLineMargin = 15

  canvas: any
  @Input() CanvasBackgroundColor = '#3cb371'
  @Input() CanvasLandscapeHeight = 20
  @Input() CanvasLandscapeWidth = 40
  @Input() CanvasPadding = 4
  @Input() CanvasPortraitHeight = 30

  @Input() CanvasPortraitWidth = 60
  coordinateGenerator: CoordinateGenerator
  ctx: CanvasRenderingContext2D

  // set starting values
  fps: number

  @Input() Interpolator: IInterpolator = new AccelerateDecelerateInterpolator()

  lastPoint: IPoint
  @Input() LineHeight = 2

  @Input() LineWidth = 15
  looper: LooperDuration
  nextBallDelay: number

  constructor() {
    this.fps = 10
    this.animationDuration = 2750
    this.nextBallDelay = (this.animationDuration / 100) * 4

    document.documentElement.style.setProperty(
      '--canvasPortraitHeight',
      `${this.CanvasPortraitHeight}vw`
    )
    document.documentElement.style.setProperty(
      '--canvasPortraitWidth',
      `${this.CanvasPortraitWidth}vw`
    )
    document.documentElement.style.setProperty(
      '--canvasLandscapeHeight',
      `${this.CanvasLandscapeHeight}vw`
    )
    document.documentElement.style.setProperty(
      '--canvasLandscapeWidth',
      `${this.CanvasLandscapeWidth}vw`
    )
    document.documentElement.style.setProperty(
      '--canvasPadding',
      `${this.CanvasPadding}vw`
    )
    document.documentElement.style.setProperty(
      '--canvasBackgroundColor',
      `${this.CanvasBackgroundColor}`
    )
  }

  animate(): void {
    this.looper.updateLoop()
    this.draw()
  }

  // Gets the percent complete for the ball cycle
  // based on the rate
  getPercentComplete(elapsedTime: number): number {
    const rateOfChange = this.looper.gettWithTWithElapsedTime(elapsedTime)
    const b: number = this.Interpolator.getInterpolar(rateOfChange)
    const percentComplete: number = b * 100
    return percentComplete
  }

  ngAfterViewInit(): void {
    this.setCanvasDimentionsAndContext()
    this.createCoordinateGenerator()

    this.onStart()
  }

  ngOnDestroy(): void {
    this.onStop()
  }

  ngOnInit(): void {
    this.looper = new LooperDuration(this.animationDuration, this.fps)
  }

  private createBottomLine(elapsedTime: number): IBottomLine {
    // Scale the line to be the Size of ball now - Delay
    const percentCompleteMinusDelay = this.getPercentComplete(
      elapsedTime - this.BottomLineDelay
    )
    const widthOfLine: number =
      this.LineWidth * this.getBallScaleX(percentCompleteMinusDelay)

    // Get the X-axis co-ordinate for now - Delay
    const coordinateMinusDelay: IPoint = this.coordinateGenerator.getCordinate(
      percentCompleteMinusDelay
    )

    const startX: number = coordinateMinusDelay.x - widthOfLine / 2
    const startY: number = this.canvas.height / 2 + this.BottomLineMargin
    const endX: number = startX + widthOfLine
    const endY: number = this.canvas.height / 2 + this.BottomLineMargin

    return BottomLine.fromCoordinates(
      startX,
      startY,
      endX,
      endY,
      this.LineHeight,
      this.BottomLineColor
    )
  }

  private createCircle(elapsedTime: number): ICircle {
    const percentComplete: number = this.getPercentComplete(elapsedTime)
    const coordinate: IPoint = this.coordinateGenerator.getCordinate(
      percentComplete
    )
    const radius: number = this.BallRadius * this.getBallScaleX(percentComplete)
    return createCircle(Circle, coordinate, radius, this.BallColor)
  }

  private createCoordinateGenerator(): void {
    this.coordinateGenerator = new CoordinateGenerator(
      this.BallRadius,
      this.canvas.getBoundingClientRect().width,
      this.canvas.getBoundingClientRect().height,
      this.ArcWidth,
      this.ArcHeight
    )
  }

  // draw all of the balls based on the time elapsed
  private draw(): void {
    // Clear the canvas before starting
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)

    // Initialize elapsed time from the looper
    let elapsedTime = this.looper.loopElapsedTime

    // Draw each ball based on the elapsed time
    for (let i = 0; i < this.BallNumber; i++) {
      // Only draw the ball in not out of bounds
      if (elapsedTime < this.animationDuration) {
        this.drawBallView(elapsedTime)
      }
      elapsedTime = elapsedTime + this.nextBallDelay
    }
  }

  private drawBallView(elapsedTime: number): void {
    this.drawCircle(this.createCircle(elapsedTime))
    this.drawBottomLine(this.createBottomLine(elapsedTime))
  }

  private drawBottomLine(bottomLine: IBottomLine): void {
    this.ctx.beginPath()
    this.ctx.fillStyle = bottomLine.color
    this.ctx.strokeStyle = bottomLine.color
    this.ctx.moveTo(bottomLine.point0.x, bottomLine.point0.y)
    this.ctx.lineTo(bottomLine.point1.x, bottomLine.point1.y)
    this.ctx.stroke()

    // console.log(`LINE CTX: x = ${bottomLine.point0.x}, y = ${bottomLine.point0.y} to ${bottomLine.point1.x}, y = ${bottomLine.point1.y}`)
  }

  private drawCircle(circle: ICircle): void {
    this.ctx.beginPath()
    this.ctx.fillStyle = circle.color
    this.ctx.strokeStyle = circle.color
    this.ctx.arc(circle.point.x, circle.point.y, circle.radius, 0, 2 * Math.PI)
    this.ctx.fill()

    //    console.log(`CIRCLE CTX: x = ${Math.round(circle.point.x)}, y = ${Math.round(circle.point.y)}, radius ${circle.radius}`)
  }

  // Get the scale for the ball based on the percentage
  private getBallScaleX(percentComplete: number): number {
    return percentComplete <= 50
      ? percentComplete / 50
      : (100 - percentComplete) / 50
  }

  private onStart(): void {
    // Display the canvas to be displayed and
    // subscribe to the infinite loop
    this.canvas.style.display = 'flex'
    const source = interval(this.looper.loopInterval)
    this.animateSubscription = source.subscribe(() => {
      return this.animate()
    })
  }

  private onStop(): void {
    // Do not display the canvas and unsubscribe
    // from the infinite loop
    this.canvas.style.display = 'none'
    this?.animateSubscription?.unsubscribe()
  }

  private setCanvasDimentionsAndContext(): void {
    this.canvas = document.getElementById('canvas')

    const clientRect = this.canvas.parentNode.getBoundingClientRect()
    this.canvas.width = clientRect.width
    this.canvas.height = clientRect.height

    this.ctx = this.canvas.getContext('2d')
  }
}
