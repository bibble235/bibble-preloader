import { NgModule } from '@angular/core'
import { BibblePreloaderComponent } from './lib/components/bibble-preloader.component'

@NgModule({
  declarations: [BibblePreloaderComponent],
  imports: [],
  exports: [BibblePreloaderComponent],
})
export class BibblePreloaderModule {}
