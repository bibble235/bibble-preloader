# BibblePreloader

![npm](https://img.shields.io/npm/v/tiny)
![npm bundle size](https://img.shields.io/bundlephobia/min/tiny)

## Build

Run `ng build bibble-preloader` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build bibble-preloader --configuration production`, go to the dist folder `cd dist/bibble-preloader` and run `npm publish`.

## Running unit tests

Run `ng test bibble-preloader` to execute the unit tests via [Karma](https://karma-runner.github.io).
